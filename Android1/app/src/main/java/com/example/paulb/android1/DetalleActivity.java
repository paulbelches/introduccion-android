/*
@author Paul Belches
paulbelches@gmial.com
20/02/18
 */
package com.example.paulb.android1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetalleActivity extends AppCompatActivity {
    ImageView foto;
    TextView titulo;
    TextView detalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        //Referenciar el objeto
        foto = (ImageView) findViewById(R.id.imageFoto);
        titulo = (TextView) findViewById(R.id.txtTitulo);
        detalle = (TextView) findViewById(R.id.textDetalle);
        //Obtener el objeto
        Intent intent = getIntent();
        Datos obj = intent.getParcelableExtra("objeto");
        //Asignar los valores que se enviaron
        titulo.setText(obj.getTitulo());
        String s ="";
        for (int i =0; i < obj.getDetalle().size(); i++){
            s = s + obj.getDetalle().get(i).getDescripcion2() + "\n";
        }
        detalle.setText(s);
        foto.setImageResource(obj.getImagen());
    }
}
