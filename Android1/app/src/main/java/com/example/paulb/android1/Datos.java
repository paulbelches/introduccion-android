package com.example.paulb.android1;

/*
@author Paul Belches
paulbelches@gmial.com
20/02/18
 */
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by paulb on 20/02/2018.
 */

public class Datos implements Parcelable {
    private int Id;
    private String titulo;
    private String descripcion1;
    private ArrayList<Descripcion> detalle;
    private int imagen;

    public Datos(int id, String titulo, String descripcion1, ArrayList<Descripcion> detalle, int imagen) {
        Id = id;
        this.titulo = titulo;
        this.descripcion1 = descripcion1;
        this.detalle = detalle;
        this.imagen = imagen;
    }
    public void addDetalle(Descripcion nuevo){
        detalle.add(nuevo);
    }
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion1() {
        return descripcion1;
    }

    public void setDescripcion1(String descripcion1) {
        this.descripcion1 = descripcion1;
    }

    public ArrayList<Descripcion> getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrayList<Descripcion> detalle) {
        this.detalle = detalle;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    protected Datos(Parcel in) {
        Id = in.readInt();
        titulo = in.readString();
        descripcion1 = in.readString();
        detalle = in.createTypedArrayList(Descripcion.CREATOR);
        imagen = in.readInt();
    }

    public static final Creator<Datos> CREATOR = new Creator<Datos>() {
        @Override
        public Datos createFromParcel(Parcel in) {
            return new Datos(in);
        }

        @Override
        public Datos[] newArray(int size) {
            return new Datos[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(Id);
        parcel.writeString(titulo);
        parcel.writeString(descripcion1);
        parcel.writeTypedList(detalle);
        parcel.writeInt(imagen);
    }
}
