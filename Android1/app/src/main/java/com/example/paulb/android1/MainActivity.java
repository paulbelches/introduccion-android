/*
@author Paul Belches
paulbelches@gmial.com
20/02/18
 */
package com.example.paulb.android1;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listaDatos;
    ArrayList<Datos> Lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Variable con el list view
        listaDatos = (ListView) findViewById(R.id.lstDatos);

        Lista = new ArrayList<Datos>();
        //Agreagr elementos a la lista
        ArrayList<Descripcion> elementos = new ArrayList<Descripcion>();
        elementos.add(new Descripcion("Para jugar se necesita:"));
        elementos.add(new Descripcion("Una pelota"));
        Lista.add(new Datos(1,"Basketball","Basketball primera vista",elementos, R.drawable.basketball));
        Lista.add(new Datos(2,"Beisbol","Beisbol primera vista",elementos, R.drawable.beisbol));
        Lista.add(new Datos(3,"Futbol","Futbol primera vista",elementos, R.drawable.futbol));
        Lista.add(new Datos(4,"Golf","Golf primera vista",elementos, R.drawable.golf));
        Lista.add(new Datos(5,"Tenis","Tenis primera vista",elementos, R.drawable.tenis));

        //Crear el adapter y agregar los elementos de la lista
        Adaptador miadaptador = new Adaptador(getApplicationContext(),Lista);
        listaDatos.setAdapter(miadaptador);

        listaDatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Mandar la lista de objetos
                Intent paso = new Intent(getApplicationContext(), DetalleActivity.class);
                paso.putExtra("objeto", Lista.get(i));
                startActivity(paso);
            }
        });
    }
}
