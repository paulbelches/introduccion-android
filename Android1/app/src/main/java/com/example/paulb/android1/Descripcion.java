package com.example.paulb.android1;

/*
@author Paul Belches
paulbelches@gmial.com
20/02/18
 */
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by paulb on 22/02/2018.
 */

public class Descripcion implements Parcelable {

    private String descripcion2;

    protected Descripcion(Parcel in) {
        descripcion2 = in.readString();
    }

    public Descripcion(String descripcion2) {
        this.descripcion2 = descripcion2;
    }

    public String getDescripcion2() {
        return descripcion2;
    }

    public void setDescripcion2(String descripcion2) {
        this.descripcion2 = descripcion2;
    }

    public static final Creator<Descripcion> CREATOR = new Creator<Descripcion>() {
        @Override
        public Descripcion createFromParcel(Parcel in) {
            return new Descripcion(in);
        }

        @Override
        public Descripcion[] newArray(int size) {
            return new Descripcion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(descripcion2);
    }
}
