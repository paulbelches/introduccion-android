/*
@author Paul Belches
paulbelches@gmial.com
20/02/18
 */
package com.example.paulb.android1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by paulb on 20/02/2018.
 */

public class Adaptador extends BaseAdapter{

    Context contexto;
    List<Datos> ListaObjetos;

    public Adaptador(Context contexto, List<Datos> listaObjetos) {
        this.contexto = contexto;
        ListaObjetos = listaObjetos;
    }

    @Override
    public int getCount() {
        return ListaObjetos.size();
    }

    @Override
    public Object getItem(int i) {
        return ListaObjetos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ListaObjetos.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vista = view;
        //Agregar layout list view
        LayoutInflater inflate = LayoutInflater.from(contexto);
        vista = inflate.inflate(R.layout.itemlistview,null);
        //Referenciar objetos
        ImageView imagen = (ImageView) vista.findViewById(R.id.imageView2);
        TextView titulo = (TextView) vista.findViewById(R.id.titulo);
        TextView descripcion = (TextView) vista.findViewById(R.id.descripcion);
        //Obtener valores y asignarlos
        titulo.setText( ListaObjetos.get(i).getTitulo().toString());
        descripcion.setText( ListaObjetos.get(i).getDescripcion1());
        imagen.setImageResource(ListaObjetos.get(i).getImagen());

        return vista;
    }
}
